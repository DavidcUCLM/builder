package SinBuilder;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JPanel;

public class GridBagLayout_Table {
	private JPanel m_table = new JPanel();

	public GridBagLayout_Table(String[][] matrix) {
		GridBagConstraints c = new GridBagConstraints();

		m_table.setLayout(new GridBagLayout());
		m_table.setBackground(Color.white);

		for (int i = 0; i < matrix.length; ++i)
			for (int j = 0; j < matrix[i].length; ++j) {
				c.gridx = i;
				c.gridy = j;
				m_table.add(new Label(matrix[i][j]), c);
			}
	}

	public Component get_table() {
		return m_table;
	}

	public static String[][] read_data_file(String file_name) {
		String[][] matrix = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file_name));
			String line;
			String[] cells;

			if ((line = br.readLine()) != null) {
				cells = line.split("\\t");
				int width = Integer.parseInt(cells[0]);
				int height = Integer.parseInt(cells[1]);
				matrix = new String[width][height];
			}

			int row = 0;
			while ((line = br.readLine()) != null) {
				cells = line.split("\\t");
				for (int i = 0; i < cells.length; ++i) {
					matrix[i][row] = cells[i];
				}
				row++;
			}
			br.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return matrix;
	}
}
